from openpyxl import load_workbook
from fuzzywuzzy import fuzz

book = load_workbook("c_name_and_medicine_name_list.xlsx")
c_name = book["Sheet2"]
medicine_name = book["Sheet3"]
count = 1

for c_name_row in c_name:
    for medicine_name_row in medicine_name:
        for c_name_cell in c_name_row:
            for medicine_name_cell in medicine_name_row :
                if fuzz.token_sort_ratio(c_name_cell.value, medicine_name_cell.value) > 90:
                    print(count,".",c_name_cell.value," ~= ", medicine_name_cell.value)
                    count = count + 1

print("Total No. of Similar Items: ", count)




